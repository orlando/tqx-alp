## Collection of instructions and material for tqX signal generation with X=ALP 

#### Relevant links 

Model registration https://its.cern.ch/jira/browse/AGENE-2094  

Reference paper: arXiv:2012.12272  

Parameters configured in our setup 
- kUL23, kuR23 appearing in the top-ALP-charm vertex (different couplings for left-/right-handed quarks)
- kUL13, kuR13 appearing in the top-ALP-up vertex (different couplings for left-/right-handed quarks)
- kDL33, kdR33 appearing in the ALP-b-b vertex 
- Ma mass of the ALP 

#### Set-up and run

Currently simple set-up, chain:  
    
    source setup_release.sh
    python test_all_jos.py (not yet functional)
    source run_locally.sh 
    
Can also run on the grid by typing (Work in progress, not implemented yet)
    
    source setup_release.sh
    lsetup panda
    python  test_all_jos_grid.py

#### Produce Feynman diagrams 
Before this line https://gitlab.cern.ch/orlando/tqx-alp/-/blob/master/MadGrapControl_tqXbbALP.py#L53 just add display diagrams; see here http://madgraph.phys.ucl.ac.be for more tutorials if needed. 

#### TRUTH Derivations 
(please see here for available derivation https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TruthDAOD)

Set-up

    setupATLAS
    asetup 21.2.6.0,AthDerivation,here 

(work for all MC15 at least)

Run 
    
    Reco_tf.py --inputEVNTFile EVNT.root --outputDAODFile test.pool.root --reductionConf TRUTH0
    
#### Analyse the derivations

Example of code here https://gitlab.cern.ch/htx/MCValidationTools. The code needs some updates but should still work on lxplus, I hope. 

As mentioned in the readme of the code, need to fix the configuation before running https://gitlab.cern.ch/htx/MCValidationTools/blob/master/MyTruthAnalysis/Root/MyxAODTruthAnalysis.cxx#L35. PROCESS can be GENERIC.  
    
    
#### Relevant parameters value

In general you can read the coupling values from here MG5_aMC_v2_3_3/models/ALP_FV_NLO_UFO/parameters.py. Alternatively you can look at the Jira ticket of the model registration to pickup the exact parameters/couplings name from the code.  
    
#### Samples commands

Save the JO you would like to run in 100xxx/10000 folder, then type  

    Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=500 --outputEVNTFile=evgen.root
    
to run locally or 

    pathena --trf "Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=500 --outputEVNTFile %OUT.EVNT.root" --outDS user.orlando.gen_test_fv_1 --extFile YourAwesomeJO.py

to run on the grid.  
