#---------------------------------------------------
# on-the-fly generation of FV heavy Higgs MG5 events
#---------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import fnmatch
import os
import sys

mode=0
nevents=int(2.5*runArgs.maxEvents)

# Initial pdf set NNPDF23_lo_as_0130_qed (247000)
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf': 315000, # NNPDF31_lo_as_0118 as nominal pdf set
    'pdf_variations':[315000], # NNPDF31_nnlo_as_0118 variations                                   
    'alternative_pdfs':[247000,263000,247000,315200], # NNPDF23_lo_as_0130_qed, NNPDF30_lo_as_0130, NNPDF30_lo_as_0118, NNPDF31_lo_as_0130
    'scale_variations':[0.5,1,2], # muR and muF variations (6-points scale variations)                
}

process_string=str(0)

# Input process comes from individual JO
if input_process == 'tqx-tc':
    process_string='generate p p > t t~, (t > w+ b, w+ > wdec wdec), (t~ > alp c~, alp > b b~) \n add process p p > t t~, (t~ > w- b~, w- > wdec wdec), (t > alp c, alp > b b~)'
    #process_string='generate p p > t t~, (t~ > alp c~, alp > b b~)'
else: 
    raise RuntimeError("Process %s not recognised in these jobOptions."%input_process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_str="""
set group_subprocesses Auto
set ignore_six_quark_processes False   
set loop_optimized_output True
set complex_mass_scheme False
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~       
define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~   
define zdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~   
define qdec = u c u~ c~
import model ALP_FV_NLO_UFO --modelname              
"""+process_string+"""
output -f
"""

#--------------------------------------------------------------
# Masses in GeV, plus model specific couplings
#--------------------------------------------------------------
###
# Parameters in the model parsed via JO
# kUL23, kuR23 appearing in the top-ALP-charm vertex
# kUL13, kuR13 appearing in the top-ALP-up vertex

model_pars_str = str(jofile) 
kUL23=int(0) 
kuR23=int(0) 
kUL13=int(0) 
kuR13=int(0) 
kDL33=int(1) 
kdR33=int(1) 
Ma=int(120)

for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    if 'Ma' in s:
        ss=s.replace("Ma","")  
        if ss.isdigit():
            Ma = int(ss)        
            print  "ALP mass is set to %i"%Ma
    if 'kUL23' in s:
        ss=s.replace("kUL23","")
        if ss.isdigit():
            kUL23 = int(ss)
            print  "kUL23 coupling set to %i"%kUL23
    if 'kuR23' in s:
        ss=s.replace("kuR23","")
        if ss.isdigit():
            kuR23 = int(ss)
            print  "kuR23 coupling set to %i"%kuR23
    if 'kUL13' in s:
        ss=s.replace("kUL13","")
        if ss.isdigit():
            kUL13 = int(ss)
            print  "kUL13 coupling set to %i"%kUL13
    if 'kuR13' in s:
        ss=s.replace("kuR13","")
        if ss.isdigit():
            kuR13 = int(ss)
            print  "kuR13 coupling set to %i"%kuR13
    if 'kDL33' in s:
        ss=s.replace("kDL33","")
        if ss.isdigit():
            kDL33 = int(ss)
            print  "kDL33 coupling set to %i"%kDL33
    if 'kdR33' in s:
        ss=s.replace("kdR33","")
        if ss.isdigit():
            kdR33 = int(ss)
            print  "kdR33 coupling set to %i"%kdR33

extras  = { 'lhe_version':'2.0',
            'cut_decays':'F',
            'scale':'125',
            'dsqrt_q2fact1':'125',
            'dsqrt_q2fact2':'125',
            'nevents' :int(nevents) }

process_dir = new_process(process_str)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

c_fact=2000.
masses={'9000005':str(Ma)+' # Ma'}
parameters={'kUL23': c_fact*int(kUL23), 'kuR23': c_fact*int(kuR23), 
            'kUL13': c_fact*int(kUL13), 'kuR13': c_fact*int(kuR13),
            #'kDL33': c_fact*int(kDL33), 'kdR33': c_fact*int(kdR33)} 
            'kdR33': c_fact*int(kdR33)} 
params={}
params['MASS']=masses
params['alpparams']=parameters
modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Lepton filter
#if not hasattr(filtSeq,"LeptonFilter"):
#    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#    filtSeq += LeptonFilter()
#    filtSeq.LeptonFilter.Ptcut = 15000.0#MeV

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.description = 'MadGraph control flavour violating ALP model'
evgenConfig.keywords+=['BSMHiggs']
evgenConfig.contact = ['Nicola Orlando <nicola.orlando@cern.ch>']
